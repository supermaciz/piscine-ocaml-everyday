(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_ref.ml                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/22 21:30:04 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/23 00:29:52 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type 'a ft_ref = { mutable contents: 'a }

let return my_val =
    { contents = my_val }

let get my_var = my_var.contents

let set my_var new_val = my_var.contents <- new_val

let bind my_var f = return (f my_var.contents)


let () =
    let ex = return 42 in begin
        Printf.printf "ex: %d\n" (get ex);
        set ex 25000;
        Printf.printf "ex new value after set: %d\n" (get ex);
        let ex2 = bind ex (fun x -> x / 2) in
        Printf.printf "ex2 new value after bind ex (fun x -> x/2): %d\n" (get ex2)
    end
