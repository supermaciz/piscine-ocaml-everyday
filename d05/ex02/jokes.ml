(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   jokes.ml                                           :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/23 00:59:52 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/23 02:20:04 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

(*
 * Native compilation : ocamlopt unix.cmxa micronap.ml
 * Bytecode compilation : ocamlc unix.cma micronap.ml
 *)

let get_joke () =
    let jokes = [|
        "Chemist who falls in acid is absorbed in work.";
        ("Q: How do you know when it's time to wash the dishes?\n" ^
        "A:  Look inside your pants; if you have a penis, it's not time.");
        "optimist, n.: A man who makes a motel reservation before a blind date.";
        "Q: Which sexual position produces the ugliest children?\nA: Ask your mother!";
        "We call our dog Egypt, because in every room he leaves a pyramid.";
        "Yo mama so old I told her to act her own age, and she died." |]
    in
    jokes.(Random.int (Array.length jokes))

let () =
    Random.init (int_of_float (Unix.time ()));
    print_endline (get_joke ())
