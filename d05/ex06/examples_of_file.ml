(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   examples_of_file.ml                                :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/23 17:55:38 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/23 23:16:30 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

(*
 * Native compilation : ocamlopt str.cmxa  examples_of_file.ml
 * Bytecode compilation : ocamlc str.cma examples_of_file.ml
 *
 * File format: '/' is the separator
 *
 * http://blog.camlcity.org/blog/bytes1.html
 * String and bytes can be used interchangably by default in OCaml 4.02.
 * Use -safe-string option to see string and bytes as two completely separate types.
 *)

let gen_result_pair lst =
    let filter_nb x = Str.string_match (Str.regexp "^-?[0-9]+.[0-9]+$") x 0 in
    let numbers_tmp = List.filter filter_nb lst in
    let numbers = List.map float_of_string numbers_tmp in
    let letter = List.filter (fun x -> Str.string_match (Str.regexp "^[a-z|A-Z]$") x 0) lst
    in
    try
        (Array.of_list numbers), (List.hd letter)
    with
    | _ -> begin
        print_endline "Error: wrong file format ?";
        exit 1
    end

let examples_of_file name =
    try
        let ic = open_in name in
        let rec loop result =
            begin try
                let str = input_line ic in
                let splitted = Str.split (Str.regexp ",") str in begin
                    loop (result @ [(gen_result_pair splitted)])
                end
            with
            | End_of_file -> result
            (*| _ -> begin print_endline "Error"; [] end*)
            end
        in
        let result = loop [] in begin
            close_in ic;
            result
        end
    with
    | Sys_error err -> begin Printf.printf "Error: the program can't open the file. %s\n" err; [] end


let () =
    if (Array.length Sys.argv) <> 2 then
        Printf.printf "Usage: %s file\n" Sys.argv.(0)
    else begin
        let rec loop lst =
            match lst with
            | [] -> ()
            | head :: tail -> begin
                print_string "([| ";
                let rec loop_array my_array i =
                    begin
                        match i with
                        | x when x = (Array.length (fst head)) -> ()
                        | _ -> begin
                            Printf.printf "%f; " my_array.(i);
                            loop_array my_array (i + 1)
                        end
                    end
                in
                loop_array (fst head) 0;
                Printf.printf "|], \"%s\")\n" (snd head);
                loop tail
            end
        in
        loop (examples_of_file Sys.argv.(1))
    end
