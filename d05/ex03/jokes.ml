(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   jokes.ml                                           :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/23 00:59:52 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/23 23:01:04 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

(*
 * Native compilation : ocamlopt str.cmxa unix.cmxa jokes.ml
 * Bytecode compilation : ocamlc str.cma unix.cma jokes.ml
 *
 * File format: '/' is the separator
 *)

let read_jokes_file name =
    try
        let ic = open_in name in
        begin try
            let str = really_input_string ic (in_channel_length ic) in
            let splitted = Str.split (Str.regexp "/") str in begin
                close_in ic;
                (Array.of_list (List.filter (fun x -> x <> "" ) splitted))
            end
        with
        | End_of_file -> begin print_endline "Reached end of file"; [||] end
        | _ -> begin print_endline "Error"; [||] end
        end
    with
    | _ -> begin print_endline "Error: the program can't open the file"; [||] end


let get_joke jokes = jokes.(Random.int (Array.length jokes))


let () =
    Random.init (int_of_float (Unix.time ()));
    if (Array.length Sys.argv) <> 2 then
        Printf.printf "Usage: %s file\n" Sys.argv.(0)
    else begin
        let jokes = read_jokes_file Sys.argv.(1) in 
        begin
            if jokes = [||] then print_endline "Error: No jokes"
            else print_endline (get_joke jokes)
        end
    end
