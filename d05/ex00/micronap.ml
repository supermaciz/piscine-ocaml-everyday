(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   micronap.ml                                        :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/22 18:30:34 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/22 23:01:46 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

(*
 * Native compilation : ocamlopt unix.cmxa micronap.ml
 * Bytecode compilation : ocamlc unix.cma micronap.ml
 *)

let checkDigits str =
    let is_digit c = if c >= '0' && c <= '9' then true else false in
    let len = (String.length str) in
    let i = ref 0 in
    if len = 0  || len > 8 then false
    else begin
        while !i < len && (is_digit (str.[!i])) do
            incr i
        done;
        if !i = len then true else false
    end

let mysleep () = Unix.sleep 1

let sleepTime sec =
    let time_sec = try int_of_string sec with
    | _ -> ~-1
    in
    for i = 1 to time_sec do
        mysleep ()
    done

let () =
    if (Array.length Sys.argv) <> 2 then ()
    else if (checkDigits Sys.argv.(1)) then sleepTime Sys.argv.(1)
    else ()
