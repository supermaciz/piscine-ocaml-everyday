(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   eu_dist.ml                                         :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/23 05:13:33 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/23 05:56:23 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let eu_dist a b =
    let sq a = a *. a in
    let size = Array.length a in
    let rec loop1 result i =
        match i with
        | x when x = size -> result
        | x -> begin result.(x) <- (a.(x) -. b.(x)); loop1 result (i + 1) end
    in
    let rec sum_array x i result =
        match i with
        | a when a = size -> result
        | _ -> sum_array x (i + 1) (result +. (sq x.(i)))
    in
    sqrt (sum_array (loop1 (Array.make size 0.0) 0) 0 0.0)


let () =
    print_string "eu_dist [|0.0;3.0;4.0;5.0|] [|7.0;6.0;3.0;-1.0|] : ";
    print_float (eu_dist [|0.0;3.0;4.0;5.0|] [|7.0;6.0;3.0;~-.1.0|]);
    print_string "\neu_dist [|2.5; 0.0|] [|4.0; 7.7|] : ";
    print_float (eu_dist [|2.5; 0.0|] [|4.0; 7.7|]);
    print_string "\neu_dist [|0.0; 0.0|] [|1.0; 1.0|] : ";
    print_float (eu_dist [|0.0; 0.0|] [|1.0; 1.0|]);
    print_char '\n'
