(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   encode.ml                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/18 02:04:13 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/18 20:05:59 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let encode (lst: 'a list) =
    let rec encode_helper (lst: 'a list) (pair: int * 'a) (result: (int * 'a) list) = match lst with
        | [] -> []
        | a :: [] ->
                begin match pair with
                    | (nb, t) when nb <> 0 && t = a -> (result @ [(nb + 1), a])
                    | (0, _) -> (1, a) :: []
                    | (_, _) -> result @ ([pair] @ [1, a])
                end
        | a :: tail ->
                begin match pair with
                    | (nb, t) when nb <> 0 && t = a -> encode_helper tail ((nb + 1), a) result
                    | (0, _) -> encode_helper tail (1, a) result
                    | (_, _) -> encode_helper tail (1, a) (result @ [pair])
                end
    in
    match lst with
        | t :: tail when tail <> [] -> encode_helper lst (0, t) []
        | t :: [] -> encode_helper lst (0, t) []
        | [] -> []
        | _ -> []

(* Our "test main" starts here *)

(* explode : string -> (int * char) list *)
let explode s =
    let rec exp i l =
        if i < 0 then l else exp (i - 1) (s.[i] :: l) in
    exp (String.length s - 1) []

let print_char_list  lst =
    let rec loop lst = match lst with
        | [] -> print_endline ""
        | (a, b) :: tail -> begin
            print_string "Number: "; print_int a;
            print_string " .Data: "; print_char b;
            print_endline "";
            loop tail
        end
    in
    loop lst

let print_int_list  lst =
    let rec loop lst = match lst with
        | [] -> print_endline ""
        | (a, b) :: tail -> begin
            print_string "Number: "; print_int a;
            print_string " .Data: "; print_int b;
            print_endline "";
            loop tail
        end
    in
    loop lst

let () =
    print_endline "Test \"LLodpfjrff\" in a list of char:"; print_char_list (encode (explode "LLodpfjrff"));
    print_endline "Test [5;5;5;1;2;2;3]:"; print_int_list (encode [5;5;5;1;2;3]);
    print_endline "Test []:"; print_int_list (encode [])
