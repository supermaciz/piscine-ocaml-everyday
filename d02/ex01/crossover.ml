(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   crossover.ml                                       :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/18 07:11:11 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/18 22:18:09 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let crossover lst1 lst2 =
    let rec search_lst2 lst elem =
        match lst with
        | [] -> false
        | head :: tail when head = elem -> true
        | head :: tail -> search_lst2 tail elem
    in
    let rec check_lst1 lst result =
        match lst with
        | [] -> result
        | head :: tail when (search_lst2 lst2 head) -> check_lst1 tail (result @ [head])
        | head :: tail -> check_lst1 tail result
    in
    check_lst1 lst1 []


(*---- TEST ----*)

let print_int_list  lst =
    let rec loop lst = 
        match lst with
        | [] -> print_endline ""
        | head :: [] -> print_int head; print_endline ""
        | head :: tail -> begin
            print_int head; print_string "; ";
            loop tail
        end
    in
    loop lst

let () =
    print_endline "Test [2;4;67;1;9;10] et [4;765;10;1;1024;256]:";
    print_int_list (crossover [2;4;67;1;9;10] [4;765;10;1;1024;256]);
    print_endline "\nTest [11; 4; 42] et []:";
    print_int_list (crossover [11; 4; 42] [])

