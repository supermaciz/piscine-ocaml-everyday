(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   atom.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/26 12:54:40 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/26 15:59:15 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

class virtual atom =
    object (self)
        method virtual get_name: string
        method virtual get_symbol: string
        method virtual get_atomic_number: int
        method to_string: string =
            self#get_name ^ " " ^ self#get_symbol ^ ": " ^ (string_of_int self#get_atomic_number)
        method equals (that: atom) =
            (that#get_name = self#get_name) && (that#get_symbol = self#get_symbol)
            && (that#get_atomic_number = self#get_atomic_number)
    end

class hydrogen =
    object
        inherit atom
        val _name: string = "hydrogen"
        val _symbol: string = "H"
        val _atomic_number: int = 1

        method get_name = _name
        method get_symbol = _symbol
        method get_atomic_number = _atomic_number
    end

class carbon =
    object
        inherit atom
        val _name: string = "carbon"
        val _symbol: string = "C"
        val _atomic_number: int = 6

        method get_name = _name
        method get_symbol = _symbol
        method get_atomic_number = _atomic_number
    end

class oxygen =
    object
        inherit atom
        val _name: string = "oxygen"
        val _symbol: string = "O"
        val _atomic_number: int = 8

        method get_name = _name
        method get_symbol = _symbol
        method get_atomic_number = _atomic_number
    end

class helium =
    object
        inherit atom
        val _name: string = "helium"
        val _symbol: string = "He"
        val _atomic_number: int = 2

        method get_name = _name
        method get_symbol = _symbol
        method get_atomic_number = _atomic_number
    end

class sodium =
    object
        inherit atom
        val _name: string = "sodium"
        val _symbol: string = "Na"
        val _atomic_number: int = 11

        method get_name = _name
        method get_symbol = _symbol
        method get_atomic_number = _atomic_number
    end

class potassium =
    object
        inherit atom
        val _name: string = "potasium"
        val _symbol: string = "K"
        val _atomic_number: int = 19

        method get_name = _name
        method get_symbol = _symbol
        method get_atomic_number = _atomic_number
    end

class silicon =
    object
        inherit atom
        val _name: string = "silicon"
        val _symbol: string = "Si"
        val _atomic_number: int = 14

        method get_name = _name
        method get_symbol = _symbol
        method get_atomic_number = _atomic_number
    end

class mercury =
    object
        inherit atom
        val _name: string = "mercury"
        val _symbol: string = "Hg"
        val _atomic_number: int = 80

        method get_name = _name
        method get_symbol = _symbol
        method get_atomic_number = _atomic_number
    end

class nitrogen = (* Azote *)
    object
        inherit atom
        val _name: string = "nitrogen"
        val _symbol: string = "N"
        val _atomic_number: int = 7

        method get_name = _name
        method get_symbol = _symbol
        method get_atomic_number = _atomic_number
    end

