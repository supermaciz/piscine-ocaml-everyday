(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   molecule.ml                                        :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/26 13:53:22 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/26 23:41:52 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

class virtual molecule =
    object (self)
        method virtual get_name: string
        method virtual get_formula: string
        method virtual get_mol_data: (Atom.atom * int) list

        method to_string: string = self#get_name ^ " " ^ self#get_formula
        method equals (that: molecule): bool =
            let rec loop atoms_lst =
                match atoms_lst with
                | [] -> true
                | head :: tail -> begin
                    try
                        let nb = (List.assoc (fst head) (that#get_mol_data)) in
                        if (snd head) = nb then loop tail else false
                    with
                    | Not_found -> false
                    | _ -> false
                end
            in
            loop self#get_mol_data

        method private mol_part_str (y: Atom.atom * int): string =
            let tmp_atom = (fst y) in
            tmp_atom#get_symbol ^ (string_of_int (snd y))
    end

class water = 
    object (self)
        inherit molecule
        val _name: string = "water"
        val _formula: string = "H2O"
        val _mol_data: (Atom.atom * int) list = [(new Atom.hydrogen, 2); (new Atom.oxygen, 1)]
        
        method get_name: string = _name
        method get_formula: string = _formula
        (*method get_formula: string =
            let tmp_carbon = new Atom.carbon in
            let tmp_hydrogen = new Atom.hydrogen in
            let carbon_formula () =
                if List.mem_assoc tmp_carbon _mol_data then
                    tmp_carbon#get_symbol ^ (string_of_int (List.assoc tmp_carbon _mol_data))
                else ""
            in
            let hydrogen_formula result =
                if List.mem_assoc tmp_hydrogen _mol_data then
                    result ^ tmp_hydrogen#get_symbol ^ (string_of_int (List.assoc tmp_hydrogen _mol_data))
                else result ^ ""
            in
            let tmp_res = hydrogen_formula (carbon_formula ()) in
            let rec loop_alpha lst score =
                match lst with
                | [] -> score
                | head :: tail ->
                    let x = String.compare "A" head#get_symbol in
                    if x < score then loop_alpha tail x else loop_alpha tail score
            in
            let rec final_loop lst result =
                match lst with
                | [] -> result
                | head :: tail -> begin
                    let rec find_pair lst =
                        if List.hd lst
                end*)

        method get_mol_data: (Atom.atom * int) list = _mol_data
    end

class carbon_dioxyde = 
    object (self)
        inherit molecule
        val _name: string = "carbon dioxyde"
        val _formula: string = "CO2"
        val _mol_data: (Atom.atom * int) list = [(new Atom.carbon, 1); (new Atom.oxygen, 2)]
        
        method get_name: string = _name
        method get_formula: string = _formula
        method get_mol_data: (Atom.atom * int) list = _mol_data
    end

class tnt = 
    object (self)
        inherit molecule
        val _name: string = "trinitrotoluene"
        val _formula: string = "C7H5N3O6"
        val _mol_data: (Atom.atom * int) list =
            [(new Atom.carbon, 7); (new Atom.hydrogen, 5); (new Atom.nitrogen, 3); (new Atom.oxygen, 6)]
        
        method get_name: string = _name
        method get_formula: string = _formula
        method get_mol_data: (Atom.atom * int) list = _mol_data
    end

class lsd = 
    object (self)
        inherit molecule
        val _name: string = "lysergic acid diethylamide"
        val _formula: string = "C20H25N3O"
        val _mol_data: (Atom.atom * int) list =
            [(new Atom.carbon, 20); (new Atom.hydrogen, 25); (new Atom.nitrogen, 3); (new Atom.oxygen, 1)]
        
        method get_name: string = _name
        method get_formula: string = _formula
        method get_mol_data: (Atom.atom * int) list = _mol_data
    end

class nitroglycerin = 
    object (self)
        inherit molecule
        val _name: string = "nitroglycerin"
        val _formula: string = "C3H5N3O9"
        val _mol_data: (Atom.atom * int) list =
            [(new Atom.carbon, 3); (new Atom.hydrogen, 5); (new Atom.nitrogen, 3); (new Atom.oxygen, 9)]
        
        method get_name: string = _name
        method get_formula: string = _formula
        method get_mol_data: (Atom.atom * int) list = _mol_data
    end

