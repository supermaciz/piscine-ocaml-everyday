(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/26 13:42:22 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/26 13:49:39 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let () =
    let h = new Atom.hydrogen in
    let c = new Atom.carbon in
    let o = new Atom.oxygen in
    let he = new Atom.helium in
    let na = new Atom.sodium in
    let k = new Atom.potassium in
    let si = new Atom.silicon in
    let hg = new Atom.mercury in
    Printf.printf "%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n" h#to_string c#to_string o#to_string he#to_string na#to_string k#to_string si#to_string hg#to_string
