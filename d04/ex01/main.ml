(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/20 17:41:42 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/20 18:11:14 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let main () =
    Printf.printf "toInt T2: %d\n" (Value.toInt Value.T2);
    Printf.printf "toInt King: %d\n" (Value.toInt Value.King);
    Printf.printf "toString King: %s\n" (Value.toString Value.King);
    Printf.printf "toStringVerbose King: %s\n" (Value.toStringVerbose Value.King);
    Printf.printf "next Jack: %s\n" (Value.toStringVerbose (Value.next Value.Jack));
    Printf.printf "previous T10: %s\n" (Value.toStringVerbose (Value.previous Value.T10));
    Printf.printf "previous T2: %s\n" (Value.toStringVerbose (Value.previous Value.T2))

let () = main ()

