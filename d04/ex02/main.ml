(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/20 17:41:42 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/21 00:01:37 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let main () =
    let tata = Card.newCard Card.Value.Jack  Card.Color.Spade in
    Printf.printf "Test: %s\n" (Card.toStringVerbose tata);
    let tata = Card.newCard Card.Value.Jack  Card.Color.Spade in
    let popo = Card.newCard Card.Value.Queen  Card.Color.Spade in
    Printf.printf "Test max: %s\n" (Card.toStringVerbose (max tata popo));
    let tata = Card.newCard Card.Value.Jack  Card.Color.Spade in
    let popo = Card.newCard Card.Value.Queen  Card.Color.Spade in
    Printf.printf "Test max: %s\n" (Card.toStringVerbose (max tata popo));
    let popo = Card.newCard Card.Value.Queen  Card.Color.Spade in
    Printf.printf "is of: %s\n" (string_of_bool (Card.isOf popo Card.Color.Diamond));
    let prout =
    [
        { prout.Card.color = Card.Color.Spade ; prout.Card.value = Card.Value.T2 };
        { prout.color = Card.Color.Spade ; Card.value = Card.Value.T3 };
        { Card.color = Card.Color.Spade ; Card.value = Card.Value.T4 };
        { Card.color = Card.Color.Spade ; Card.value = Card.Value.T5 };
        { Card.color = Card.Color.Spade ; Card.value = Card.Value.T6 };
        { Card.color = Card.Color.Spade ; Card.value = Card.Value.T10 };
        { Card.color = Card.Color.Spade ; Card.value = Card.Value.T10 };
        { Card.color = Card.Color.Spade ; Card.value = Card.Value.Jack };
        { Card.color = Card.Color.Spade ; Card.value = Card.Value.Queen };
        { Card.color = Card.Color.Spade ; Card.value = Card.Value.King };
        { Card.color = Card.Color.Spade ; Card.value = Card.Value.As }
    ]
    in
    Printf.printf "Test: %s\n" (Card.toStringVerbose (Card.best prout))
    


let () = main ()

