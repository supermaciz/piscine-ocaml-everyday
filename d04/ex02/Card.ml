(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Card.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/20 18:21:28 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/20 23:30:23 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

module Color =
    struct
        type t = Spade | Heart | Diamond | Club

        let all = [Spade; Heart; Diamond; Club]

        let toString tmp =
            match tmp with
            | Spade -> "S"
            | Heart -> "H"
            | Diamond -> "D"
            | Club -> "C"

        let toStringVerbose tmp =
            match tmp with
            | Spade -> "Spade"
            | Heart -> "Heart"
            | Diamond -> "Diamond"
            | Club -> "Club"
    end


module Value =
    struct
        type t = T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | Jack | Queen | King | As

        let all = [T2; T3; T4; T5; T6; T7; T8; T9; T10; Jack; Queen; King; As]

        let toInt card =
            match card with
            | T2 -> 1
            | T3 -> 2
            | T4 -> 3
            | T5 -> 4
            | T6 -> 5
            | T7 -> 6
            | T8 -> 7
            | T9 -> 8
            | T10 -> 9
            | Jack -> 10
            | Queen -> 11
            | King -> 12
            | As -> 13

        let toString card =
            match card with
            | T2 -> "2"
            | T3 -> "3"
            | T4 -> "3"
            | T5 -> "5"
            | T6 -> "6"
            | T7 -> "6"
            | T8 -> "8"
            | T9 -> "8"
            | T10 -> "10"
            | Jack -> "J"
            | Queen -> "Q"
            | King -> "K"
            | As -> "A"

        let toStringVerbose card =
            match card with
            | T2 -> "2"
            | T3 -> "3"
            | T4 -> "3"
            | T5 -> "5"
            | T6 -> "6"
            | T7 -> "6"
            | T8 -> "8"
            | T9 -> "9"
            | T10 -> "10"
            | Jack -> "Jack"
            | Queen -> "Queen"
            | King -> "King"
            | As -> "As"

        let next card =
            match card with
            | T2 -> T3
            | T3 -> T4
            | T4 -> T5
            | T5 -> T6
            | T6 -> T7
            | T7 -> T8
            | T8 -> T9
            | T9 -> T10
            | T10 -> Jack
            | Jack -> Queen
            | Queen -> King
            | King -> As
            | As -> invalid_arg "Nothing after As"

        let previous card =
            match card with
            | T2 -> invalid_arg "Nothing before T2"
            | T3 -> T2
            | T4 -> T3
            | T5 -> T4
            | T6 -> T5
            | T7 -> T6
            | T8 -> T7
            | T9 -> T8
            | T10 -> T9
            | Jack -> T10
            | Queen -> Jack
            | King -> Queen
            | As -> King
    end

type t = {
    color : Color.t;
    value : Value.t
}
 

let newCard card_val card_color = {
    color = card_color;
    value = card_val
}

let allSpades =
    [
        { color = Color.Spade ; value = Value.T2 };
        { color = Color.Spade ; value = Value.T3 };
        { color = Color.Spade ; value = Value.T4 };
        { color = Color.Spade ; value = Value.T5 };
        { color = Color.Spade ; value = Value.T6 };
        { color = Color.Spade ; value = Value.T7 };
        { color = Color.Spade ; value = Value.T8 };
        { color = Color.Spade ; value = Value.T9 };
        { color = Color.Spade ; value = Value.T10 };
        { color = Color.Spade ; value = Value.Jack };
        { color = Color.Spade ; value = Value.Queen };
        { color = Color.Spade ; value = Value.King };
        { color = Color.Spade ; value = Value.As }
    ]


let allHearts =
    [
        { color = Color.Heart ; value = Value.T2 };
        { color = Color.Heart ; value = Value.T3 };
        { color = Color.Heart ; value = Value.T4 };
        { color = Color.Heart ; value = Value.T5 };
        { color = Color.Heart ; value = Value.T6 };
        { color = Color.Heart ; value = Value.T7 };
        { color = Color.Heart ; value = Value.T8 };
        { color = Color.Heart ; value = Value.T9 };
        { color = Color.Heart ; value = Value.T10 };
        { color = Color.Heart ; value = Value.Jack };
        { color = Color.Heart ; value = Value.Queen };
        { color = Color.Heart ; value = Value.King };
        { color = Color.Heart ; value = Value.As }
    ]

let allDiamonds =
    [
        { color = Color.Diamond ; value = Value.T2 };
        { color = Color.Diamond ; value = Value.T3 };
        { color = Color.Diamond ; value = Value.T4 };
        { color = Color.Diamond ; value = Value.T5 };
        { color = Color.Diamond ; value = Value.T6 };
        { color = Color.Diamond ; value = Value.T7 };
        { color = Color.Diamond ; value = Value.T8 };
        { color = Color.Diamond ; value = Value.T9 };
        { color = Color.Diamond ; value = Value.T10 };
        { color = Color.Diamond ; value = Value.Jack };
        { color = Color.Diamond ; value = Value.Queen };
        { color = Color.Diamond ; value = Value.King };
        { color = Color.Diamond ; value = Value.As }
    ]

let allClubs =
    [
        { color = Color.Club ; value = Value.T2 };
        { color = Color.Club ; value = Value.T3 };
        { color = Color.Club ; value = Value.T4 };
        { color = Color.Club ; value = Value.T5 };
        { color = Color.Club ; value = Value.T6 };
        { color = Color.Club ; value = Value.T7 };
        { color = Color.Club ; value = Value.T8 };
        { color = Color.Club ; value = Value.T9 };
        { color = Color.Club ; value = Value.T10 };
        { color = Color.Club ; value = Value.Jack };
        { color = Color.Club ; value = Value.Queen };
        { color = Color.Club ; value = Value.King };
        { color = Color.Club ; value = Value.As }
    ]

let all = allSpades @ allHearts @ allDiamonds @ allClubs

let getValue data = data.value

let getColor data = data.color

let toString data = (Value.toString data.value) ^ (Color.toString data.color)

let toStringVerbose data =
    Printf.sprintf "(%s, %s)" (Value.toStringVerbose data.value) (Color.toStringVerbose data.color)

let compare x y =
    if Value.toInt (getValue x) > Value.toInt (getValue y) then 1
    else if Value.toInt (getValue x) < Value.toInt (getValue y) then ~-1
    else 0

let max x y =
    if Value.toInt (getValue x) > Value.toInt (getValue y) then x
    else if Value.toInt (getValue x) < Value.toInt (getValue y) then y
    else x

let min x y =
    if Value.toInt (getValue x) > Value.toInt (getValue y) then y
    else if Value.toInt (getValue x) < Value.toInt (getValue y) then x
    else x

let best card_lst =
    match card_lst with
    | [] -> invalid_arg "Empty list"
    | head :: tail ->
            let rec equal_val card_lst result =
                match card_lst with
                | head :: [] -> [result]
                | [] -> [result]
                | head :: snd :: tail when head = snd -> [head]
                | head :: tail -> equal_val tail head
            in
            let toto = List.fold_left equal_val [] card_lst
            in
            match toto with
            | x :: tl -> x
            | [] -> invalid_arg "Empty list"

let isOf my_card color = (getColor my_card) = color

let isSpade my_card = (getColor my_card) = Color.Spade
let isHeart my_card = (getColor my_card) = Color.Heart
let isDiamond my_card = (getColor my_card) = Color.Diamond
let isClub my_card = (getColor my_card) = Color.Club
