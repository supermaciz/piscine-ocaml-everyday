(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/20 17:25:16 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/20 17:37:29 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let main () =
    print_string "toString Spade: "; print_endline (Color.toString Color.Spade);
    print_string "toString Diamond: "; print_endline (Color.toString Color.Diamond);
    print_string "toStringVerbose Club: ";
    print_endline (Color.toStringVerbose Color.Club);
    print_string "toStringVerbose Spade: ";
    print_endline (Color.toStringVerbose Color.Spade)

let () = main ()
