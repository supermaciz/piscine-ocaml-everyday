(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ex00.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/24 03:53:58 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/24 20:50:29 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

module StringSet = struct
    type t = string with compare, sexp
    let hash = Hashtbl.hash
end
include Comparable.Make(StringSet)
include Hashable.Make(T) 

let () =
    let set = List.fold_right StringSet.add [ "foo"; "bar"; "baz"; "qux" ] StringSet.empty in
    StringSet.iter print_endline set;
    print_endline (StringSet.fold ( ^ ) set "")
