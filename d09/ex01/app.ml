(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   app.ml                                             :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/27 17:59:17 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/27 19:36:32 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let tri_fst triplet =
    match triplet with
    | a, _, _ -> a

let tri_snd triplet =
    match triplet with
    | _, a, _ -> a

let tri_third triplet =
    match triplet with
    | _, _, a -> a


module App =
    struct
        type project = string * string * int
        let zero : project = "", "", 0

        let combine (pro1 : project) (pro2 : project) : project =
            let avg = ((tri_third pro1) + (tri_third pro2)) / 2 in
            let status = if avg > 80 then "succeed" else "failed" in
            (tri_fst pro1) ^ (tri_fst pro2), status, avg

        let fail (pro : project) : project = (tri_fst pro), "failed", 0 
        let success (pro : project) : project = (tri_fst pro), "succeed", 80 
    end

let print_proj (pro: App.project) =
    print_string "Project: "; print_endline (tri_fst pro);
    print_string "Status: "; print_endline (tri_snd pro);
    print_string "Grade: "; print_int (tri_third pro);
    print_endline "\n"



let () =
    let cpp: App.project = ("Piscine C++", "failed", 34) in
    let ft_sh1: App.project = ("Minishell1", "succeed", 115) in
    let wolf3d: App.project = "Wolf3d", "failed", 70 in
    begin
        print_proj cpp; print_proj ft_sh1; print_proj wolf3d;
        print_endline "Test combine ft_sh1 et wolf3d:";
        print_proj (App.combine ft_sh1 wolf3d);
        print_endline "Test fail ft_sh1:";
        print_proj (App.fail ft_sh1);
        print_endline "Test success C++:";
        print_proj (App.success cpp)
    end
    
