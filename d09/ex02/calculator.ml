(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   calculator.ml                                      :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/27 22:52:35 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/27 23:10:05 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

module type MONOID =
    sig
        type element
        val zero1 : element
        val zero2 : element
        val mul : element -> element -> element
        val add : element -> element -> element
        val div : element -> element -> element
        val sub : element -> element -> element
    end

module INT =
    struct
        type element = int
        let zero1 = 0
        let zero2 = 1
        let add x y = x + y
        let sub x y = x - y
        let mul x y = x * y
        let div x y = x / y
    end

module FLOAT =
    struct
        type element = float
        let zero1 = 0.0
        let zero2 = 1.0
        let add x y = x +. y
        let sub x y = x -. y
        let mul x y = x *. y
        let div x y = x /. y
    end

module Calc =
    functor (M: MONOID) ->
        struct
            let add x y = M.add x y
            let sub x y = M.sub x y
            let mul x y = M.mul x y
            let div x y = M.div x y
            let power a b =
                let rec loop acc counter =
                    match counter with
                    | 0 -> acc
                    | x -> loop (M.mul acc a) (x - 1)
                in
                loop M.zero2 b
            let fact n =
                let rec fact_helper n acc =
                    if n <= M.zero2 then acc
                    else fact_helper (M.sub n M.zero2) (M.mul acc n)
                in fact_helper n M.zero2
        end

