(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/27 22:52:56 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/27 23:39:19 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

module Calc_int = Calculator.Calc(Calculator.INT)
module Calc_float = Calculator.Calc(Calculator.FLOAT)

let () =
    Printf.printf "2 * 4 = %d\n" (Calc_int.mul 2 4);
    Printf.printf "3.0 pow 3 = %f\n" (Calc_float.power 3.0 3);
    Printf.printf "(20 + 1) * 2 = %d\n" (Calc_int.mul (Calc_int.add 20 1) 2);
    Printf.printf "(20.0 + 1.0) * 2.0 = %f\n" (Calc_float.mul (Calc_float.add 20.0 1.0) 2.0);
    Printf.printf "2.567 pow 4 = %f\n" (Calc_float.power 2.567 4);
    Printf.printf "fact 4 = %d\n" (Calc_int.fact 4);
    Printf.printf "10 / 2 = %d\n" (Calc_int.div 10 2);
    Printf.printf "10.5 / 2.3 = %f\n" (Calc_float.div 10.5 2.3);
    Printf.printf "3.0 - 1.4 = %f\n" (Calc_float.sub 3.0 1.4)
