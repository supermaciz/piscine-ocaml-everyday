(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   watchtower.ml                                      :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/27 16:42:47 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/27 17:47:15 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

module Watchtower =
    struct
        type hour = int
        
        let zero: hour = 12

        let add (x: hour) (y: hour): hour = (x + y) mod zero
        let sub (x: hour) (y: hour): hour =
            let tmp = (x - y) mod zero in
            if tmp < 1 then (add zero tmp) else tmp
    end
