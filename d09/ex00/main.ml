(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/27 17:21:34 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/27 17:53:11 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let () =
    let x: Watchtower.Watchtower.hour = 10 in
    let y: Watchtower.Watchtower.hour = 7 in
    let a: Watchtower.Watchtower.hour = 10 in
    let b: Watchtower.Watchtower.hour = 2 in
    Printf.printf "10h + 7h = %dh\n" (Watchtower.Watchtower.add x y);
    Printf.printf "10h + 2h = %dh\n" (Watchtower.Watchtower.add a b);
    Printf.printf "10h - 2h = %dh\n" (Watchtower.Watchtower.sub a b);
    Printf.printf "7h - 10h = %dh\n" (Watchtower.Watchtower.sub y x)
