(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   army.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/25 15:07:24 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/25 17:23:23 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

class ['a] army (lst: 'a list) =
    object (self)
        val _my_list = lst

        method add somebody = {< _my_list = somebody :: _my_list  >}
        method delete =
            match _my_list with
            | head :: tail -> {< _my_list = tail >}
            | [] -> self
        method get_my_list = _my_list
    end
