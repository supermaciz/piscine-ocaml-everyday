(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   dalek.ml                                           :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/25 12:23:24 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/25 18:14:08 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

class dalek =
    object
        val _name: string = begin
            let rec loop str =
                if (String.length str) = 3 then str
                else loop (str ^ (String.make 1 (char_of_int (Random.int (126-33 +1) + 33))))
            in
            "Dalek" ^ (loop "")
        end
        val _hp: int = 100
        val mutable _shield: bool = true

        initializer print_endline ("A new dalek object called " ^ _name ^ " is born.")
        method to_string =
            (_name ^ ". HP: " ^ (string_of_int _hp) ^ ". Shield :" ^ (string_of_bool _shield))
        method talk = 
            let x = Random.int 4 in
            match x with
            | 0 -> print_endline "Explain! Explain!"
            | 1 -> print_endline "Exterminate! Exterminate!"
            | 2 -> print_endline "I obey!"
            | _ -> print_endline "You are the Doctor! You are the enemy of the Daleks!"
        method die = print_endline "Emergency Temporal Shift!"
        method exterminate (dead_man: People.people) = dead_man#die; _shield <- false
        method get_name = _name
    end
