(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/24 21:50:07 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/24 21:50:44 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let () =
	let robin = new People.people "Robin" in
	let jacques = new Doctor.doctor "Jacques" 42 robin  in
	begin
		Printf.printf "Test to_string: %s\n" jacques#to_string;
		jacques#use_sonic_screwdriver;
		jacques#talk;
		Printf.printf "Test travel_in_time: %s\n" (jacques#travel_in_time 10 25)#to_string
	end
