(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/24 21:50:07 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/25 14:46:00 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let () =
    Random.init (int_of_float (Unix.time ()));
    let dodo = new Dalek.dalek in
	let robin = new People.people "Robin" in
	let jacques = new Doctor.doctor "Jacques" 42 robin  in
	begin
		jacques#talk;
        Printf.printf "Dalek's state. %s\n\n" dodo#to_string;
        Printf.printf "The dalek can talk: "; dodo#talk;
        Printf.printf "The dalek can exterminate people: "; dodo#exterminate robin;
        Printf.printf "Dalek's state. %s\n\n" dodo#to_string;
		jacques#use_sonic_screwdriver;
        dodo#die
	end
