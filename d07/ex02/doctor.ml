(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   doctor.ml                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/24 21:46:51 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/25 14:45:03 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

class doctor name age sidekick =
    object
        val _name: string = name
        val _age: int = age
        val _sidekick: People.people = sidekick
        val _hp: int = 100
        
        initializer print_endline ("A new doctor object called " ^ _name ^ " is born.")
        method to_string =
            ("Doctor " ^ _name ^ "\nHP: " ^ (string_of_int _hp) ^ "\nAge: " ^(string_of_int _age)
            ^ "\nSidekick: " ^ (sidekick#get_name) ^"\n")
        method talk = print_endline ("Hi I'm the Doctor!")
        method travel_in_time start arrival =
            print_endline "
          _
         /-\\
    _____|#|_____
   |_____________|
  |_______________|
|||_POLICE_##_BOX_|||
 | |¯|¯|¯|||¯|¯|¯| |
 | |-|-|-|||-|-|-| |
 | |_|_|_|||_|_|_| |
 | ||~~~| | |¯¯¯|| |
 | ||~~~|!|!| O || |
 | ||~~~| |.|___|| |
 | ||¯¯¯| | |¯¯¯|| |
 | ||   | | |   || |
 | ||___| | |___|| |
 | ||¯¯¯| | |¯¯¯|| |
 | ||   | | |   || |
 | ||___| | |___|| |
|¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯|
 ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯";
            new doctor _name (age + (arrival - start)) _sidekick
        method use_sonic_screwdriver = print_endline "Whiiiiwhiiiwhiii Whiiiiwhiiiwhiii Whiiiiwhiiiwhiii"
        method private regenerate = new doctor _name _age _sidekick
    end
