(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/24 21:15:25 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/24 21:39:31 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let () =
    let poulpe = new People.people("Le Poulpe") in begin
        poulpe#talk;
        Printf.printf "Test to_string. %s\n" poulpe#to_string;
        poulpe#die
    end
