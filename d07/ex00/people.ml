(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   people.ml                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/24 20:57:36 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/24 21:41:54 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

class people name =
    object
        val _name: string = name
        val _hp: int = 100

        initializer print_endline ("A new people object called " ^ _name ^ " is born.")
        method to_string = _name ^ ": " ^ (string_of_int _hp)
        method talk = print_endline ("I'm " ^ _name ^ "! Do you know the Doctor?")
        method die = print_endline "Aaaarghh!"
    end
