(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/24 21:50:07 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/25 22:08:51 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec random_name str =
    if (String.length str) = 6 then str
    else random_name (str ^ (String.make 1 (char_of_int (Random.int (90-65 +1) + 65))))

let rec create_army nb my_army new_somebody =
    match nb with
    | x when x <= 0 -> my_army
    | _ -> create_army (nb - 1) (my_army#add (new_somebody ())) new_somebody

let new_people () = new People.people (random_name "")

let new_doc sidekick = new Doctor.doctor (random_name "") 45 sidekick

let new_dalek () = new Dalek.dalek


let () =
    Random.init (int_of_float (Unix.time()));
    let people_army =
        create_army 5 (new Army.army [new_people ()]) new_people
    in
    let new_doc2 () = new_doc (List.hd (people_army#get_my_list)) in
    let doc_army =
        create_army 5 (new Army.army [new_doc2 ()]) new_doc2
    in
    let dalek_army =
        create_army 5 (new Army.army [new_dalek ()]) new_dalek
    in
    let planet = new Galifrey.galifrey dalek_army doc_army people_army in
    planet#do_time_war
