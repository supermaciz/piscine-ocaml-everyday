(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   galifrey.ml                                        :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/25 20:14:37 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/25 22:09:42 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

class galifrey dalek_army doc_army people_army =
    object (self)
        val _dalek_army: Dalek.dalek Army.army = dalek_army
        val _doc_army: Doctor.doctor Army.army = doc_army
        val _people_army: People.people Army.army = people_army

        method private create_army nb my_army new_somebody =
            let rec loop nb my_army new_somebody =
                match nb with
                | x when x <= 0 -> my_army
                | _ -> loop (nb - 1) (my_army#add (new_somebody ())) new_somebody
            in
            loop nb my_army new_somebody

        method random_name =
            let rec loop_name str =
                if (String.length str) = 6 then str
                else loop_name (str ^ (String.make 1 (char_of_int (Random.int (90-65 +1) + 65))))
            in
            loop_name ""

        method private new_people () = new People.people (self#random_name)

        method private new_doc sidekick = new Doctor.doctor (self#random_name) 42 sidekick

        method private new_dalek () = new Dalek.dalek

        method private kill_daleks = 
            let rec loop_del my_army =
                match my_army with
                | x when x#get_my_list = [] -> ()
                | x -> begin
                    let toto = List.hd (x#get_my_list) in
                    Printf.printf "%s: " toto#get_name; toto#die;
                    loop_del (x#delete)
                end
            in
            loop_del _dalek_army

        method private kill_people = 
            let rec loop_del my_army victims =
                match my_army with
                | x when x#get_my_list = [] -> ()
                | x -> begin
                    let toto = List.hd (x#get_my_list) in
                    let dead = List.hd (victims#get_my_list) in
                    Printf.printf "%s attacks and kills %s:" toto#get_name dead#get_name;
                    toto#exterminate dead;    (* The dalek 'toto' kill a people *)
                    loop_del (x#delete) (victims#delete)
                end
            in
            loop_del _dalek_army _people_army

        method private doc_sonic = 
            let rec loop lst =
                match lst with
                | [] -> ()
                | head :: tail -> begin
                    Printf.printf "%s: " head#get_name; head#use_sonic_screwdriver;
                    loop tail
                end
            in
            loop _doc_army#get_my_list
        
        method do_time_war =
            print_endline "\nLet's fight!\n";
            self#kill_people; print_endline "";
            self#doc_sonic; print_endline "";
            self#kill_daleks
            
    end
