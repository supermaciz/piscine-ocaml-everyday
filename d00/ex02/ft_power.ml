(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_power.ml                                        :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/15 14:38:30 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/15 14:50:48 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec ft_power a b =
    if b = 0
    then 1
    else if b < 0
    then 0
    else a * (ft_power a (b - 1))

let main () =
    print_endline ("Test 2^10: " ^ string_of_int (ft_power 2 10));
    print_endline ("Test 5^0: " ^ string_of_int (ft_power 5 0));
    print_endline ("Test 7^-2: " ^ string_of_int (ft_power 5 ~-2));
    print_endline ("Test 4^3: " ^ string_of_int (ft_power 4 3))

let () =  main()
