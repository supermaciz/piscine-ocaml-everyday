(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_string_all.ml                                   :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/16 23:11:13 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/16 23:33:19 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_string_all fct str =
    let len = String.length str in
    let rec loop i =
        if i < len then begin
            if fct (String.get str i) then loop (i + 1)
            else false
        end
        else true
    in
    loop 0

let is_a c = c = 'A' || c = 'a'

let () =
    if ft_string_all is_a "AAaaAaaaAAA" then print_endline "test 1: true"
    else print_endline "test 1: false";
    if ft_string_all is_a "AAA c'est faux" then print_endline "test 2: true"
    else print_endline "test 2: false"
