(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_is_palindrome.ml                                :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/16 23:35:22 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/16 23:44:24 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_is_palindrome str =
    let rec reverse_str rev_s i =
        if i = 0 then str = (rev_s ^ (String.get str i))
        else reverse_str (rev_s ^ (String.get str i)) (i - 1)
    in
    reverse_str "" (String.length str - 1)

let () =
    if ft_is_palindrome "AAaaAaaaAAA" then print_endline "test 1: true"
    else print_endline "test 1: false";
    if ft_is_palindrome "AAA c'est faux" then print_endline "test 2: true"
    else print_endline "test 2: false"
