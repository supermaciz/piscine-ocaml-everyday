(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_test_sign.ml                                    :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/15 11:57:14 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/15 14:35:08 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_test_sign x =
    if x < 0
    then print_endline "negative"
    else print_endline "positive"

let main () =
    print_string "Test with -42: ";
    ft_test_sign ~-42;
    print_string "Test with 67: ";
    ft_test_sign 67;
    print_string "Test with 9: ";
    ft_test_sign 9

let () = main ()

