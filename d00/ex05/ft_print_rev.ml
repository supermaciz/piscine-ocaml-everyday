(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_print_rev.ml                                    :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/15 20:12:23 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/16 23:02:56 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_print_rev target = 
    let rec loop i =
        if i = 0
        then begin
            print_char (String.get target i);
            print_char '\n';
        end
        else begin
            print_char (String.get target i);
            loop (i - 1)
        end
    in
    loop (String.length target - 1)

let () =
    ft_print_rev "Hello World !";
    ft_print_rev "Another test."

