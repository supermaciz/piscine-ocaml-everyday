(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_countdown.ml                                    :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/15 14:02:48 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/15 14:35:54 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec ft_countdown x =
    if x <= 0
    then print_endline ("0")
    else begin
        print_endline (string_of_int x);
        ft_countdown (x - 1)
    end

let main () =
    print_endline "Test with -3:";
    ft_countdown ~-3;
    print_endline "Test with 5:";
    ft_countdown 5

let () = main ()
