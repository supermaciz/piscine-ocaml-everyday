(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_print_alphabet.ml                               :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/15 14:53:11 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/15 15:21:34 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_print_alphabet () =
    let a_code = int_of_char 'a' in
    let z_code = int_of_char 'z' in
    let rec my_loop current_ascii = 
        if current_ascii <= z_code then begin
            print_char (char_of_int current_ascii);
            my_loop (current_ascii + 1)
        end
    in
    my_loop a_code;
    print_char '\n'

let main () = ft_print_alphabet ()

let () = main ()
