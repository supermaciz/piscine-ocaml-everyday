(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_print_comb.ml                                   :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/15 15:41:39 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/16 22:51:26 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_print_comb () =
    let init_b = 1 in
    let init_c = 2 in
    let rec loop a b start_b c start_c =
        if a <= 9
        then begin
            if a <> b && b <> c && c <> a then begin
                print_int a;
                print_int b;
                print_int c;
                if not (a = 7 && b = 8 && c = 9) then print_string ", ";
                if (a = 7 && b = 8 && c = 9) then () else
                if c < 9 then loop a b start_b (c + 1) start_c else
                if c = 9 && b < 9 then loop a (b + 1) start_b start_c (start_c + 1) else
                if c = 9 && b = 9 then loop (a + 1) init_b init_b init_c init_c
            end
            else begin
                if c < 9 then loop a b start_b (c + 1) start_c else
                if c = 9 && b < 9 then loop a (b + 1) start_b start_c (start_c + 1) else
                if c = 9 && b = 9 then loop (a + 1) init_b init_b init_c init_c
            end
        end
    in
    loop 0 1 1 2 2

let () = ft_print_comb ()

