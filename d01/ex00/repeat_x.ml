(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   repeat_x.ml                                        :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/17 01:16:11 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/17 01:50:34 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let repeat_x nb  = 
    let rec loop i str  =
        if i = 0 then str
        else if i < 0 then "Error"
        else loop (i - 1) (str ^ "x")
    in
    loop nb ""

let () =
    print_string "repeat_x 1: ";
    print_endline (repeat_x 1);
    print_string "repeat_x -42: ";
    print_endline (repeat_x ~-42);
    print_string "repeat_x 0: ";
    print_endline (repeat_x 0);
    print_string "repeat_x 6: ";
    print_endline (repeat_x 6);
