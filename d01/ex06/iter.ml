(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   iter.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/17 22:32:16 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/17 22:37:28 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec iter f x n =
	if n = 0 then x else
	if n > 0 then iter f (f x) (n - 1) else ~-1

let () =
	print_string "iter (fun x -> x*x) 2 4:"; print_int (iter (fun x -> x*x) 2 4);
	print_string "\niter (fun x -> x*2) 2 4:"; print_int (iter (fun x -> x*2) 2 4);
	print_string "\niter (fun x -> x+1) 2 4:"; print_int (iter (fun x -> x+1) 2 4);
	print_string "\niter (fun x -> x*2) 2 -2:"; print_int (iter (fun x -> x*2) 2 ~-2)
