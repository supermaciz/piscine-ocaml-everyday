(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   fibonacci.ml                                       :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/17 03:44:18 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/17 23:34:39 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let fibonacci n =
    let rec loop x y n =
        if n > 0 then loop y (x + y) (n - 1)
        else x
    in
    loop 0 1 n

let () =
    print_string "fibonacci -4 : "; print_int (fibonacci ~-4);
    print_string "\nfibonacci 4 : "; print_int (fibonacci 4);
    print_string "\nfibonacci 1 : "; print_int (fibonacci 1);
    print_string "\nfibonacci 0 : "; print_int (fibonacci 0);
    print_string "\nfibonacci 6 : "; print_int (fibonacci 6)
