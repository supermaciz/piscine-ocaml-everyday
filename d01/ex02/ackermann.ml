(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ackermann.ml                                       :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/17 02:37:39 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/17 02:57:46 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec ackermann m n =
    if m < 0 || n < 0 then -1
    else if m = 0 then (n + 1)
    else if m > 0 && n = 0 then ackermann (m - 1) 1
    else if m > 0 && n > 0 then ackermann (m - 1) (ackermann m (n - 1))
    else -1

let () =
    print_string "-1 7: "; print_int (ackermann ~-1 7);
    print_string "\n0 0: "; print_int (ackermann 0 0);
    print_string "\n2 3: "; print_int (ackermann 2 3);
    print_string "\n4 1: "; print_int (ackermann 4 1);
    print_string "\n1 5: "; print_int (ackermann 1 5);
    print_char '\n'
