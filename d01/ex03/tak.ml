(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   tak.ml                                             :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/17 03:13:51 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/17 03:39:22 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec tak x y z =
    if y < x then tak (tak (x-1) y z) (tak (y-1) z x) (tak (z-1) x y)
    else z

let () =
    print_string "tak 1 2 3: "; print_int (tak 1 2 3);
    print_string "\ntak 5 23 7: "; print_int (tak 5 23 7);
    print_string "\ntak 4 1 3: "; print_int (tak 4 1 3);
    print_string "\ntak 9 1 0: "; print_int (tak 9 1 0);
    print_string "\ntak 12 9 23: "; print_int (tak 12 9 23);
    print_string "\ntak 7 7 7: "; print_int (tak 7 7 7);
    print_char '\n'
