(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   converges.ml                                       :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/17 22:32:56 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/17 22:47:58 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec converges f x n =
    if n = 0 then begin
        if x = (f x) then true else false
    end
    else if n > 0 then converges f (f x) (n - 1) else false

let () = 
	print_string "converges (( * ) 2) 2 5:";
    if (converges (( * ) 2) 2 5) then print_string "true" else print_string "false";
	print_string "\nconverges (fun x -> x/2) 2 3:";
    if (converges (fun x -> x/2) 2 3); then print_string "true" else print_string "false";
	print_string "\nconverges (fun x -> x/2) 2 2:";
    if (converges (fun x -> x/2) 2 2) then print_string "true" else print_string "false";
	print_string "\nconverges (fun x -> x*2) 2 -2:";
    if (converges (fun x -> x*2) 2 ~-2) then print_string "true" else print_string "false";
    print_endline ""
