(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   repeat_string.ml                                   :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/17 01:54:35 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/17 02:16:01 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let repeat_string ?(str="x") nb =
    let rec loop i tmp =
        if i = 0 then tmp
        else if i < 0 then "Error"
        else loop (i - 1) (tmp ^ str)
    in
    loop nb ""

let () =
    print_string "repeat_string \"ok\" 1: ";
    print_endline (repeat_string ~str:"ok" 1);
    print_string "repeat_string \"bla\" -42: ";
    print_endline (repeat_string ~str:"bla" ~-42);
    print_string "repeat_string 0: ";
    print_endline (repeat_string 0);
    print_string "repeat_string 4: ";
    print_endline (repeat_string 4);
    print_string "repeat_string \"Ha\" 3: ";
    print_endline (repeat_string ~str:"Ha" 3);
    print_string "repeat_string \"Nothing\" 0: ";
    print_endline (repeat_string ~str:"Nothing" 0);
