(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   hofstadter_mf.ml                                   :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/06/17 22:37:46 by mcizo             #+#    #+#             *)
(*   Updated: 2015/06/17 22:38:01 by mcizo            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec hfs_f n =
	if n = 0 then 1 else
	if n > 0 then n - hfs_m (hfs_f (n - 1)) else ~-1

and hfs_m n =
	if n = 0 then 0 else
	if n > 0 then n - hfs_f (hfs_m (n - 1)) else ~-1

let () =
	print_string "hfs_m 4: "; print_int (hfs_m 4);
	print_string "\nhfs_f 4: "; print_int (hfs_f 4);
	print_string "\nhfs_f 0: "; print_int (hfs_f 0);
	print_string "\nhfs_m 0: "; print_int (hfs_m 0);
	print_string "\nhfs_m -4: "; print_int (hfs_m ~-4);
	print_string "\nhfs_f -7: "; print_int (hfs_f ~-7);
	print_string "\nhfs_m 8: "; print_int (hfs_m 8)

